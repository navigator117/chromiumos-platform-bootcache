# Copyright (c) 2011 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

include common.mk

CFLAGS += -Wformat=0

bootcache_OBJS = bootcache.o

clean: CLEAN(bootcache)
all: CC_BINARY(bootcache)

CC_BINARY(bootcache): $(bootcache_OBJS)
